﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;

namespace ProjetoAEBD
{
    public partial class IO_RW : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {   
        }

        protected void Chart1_Load(object sender, EventArgs e)
        {
            ///READS
            WebRequest request = WebRequest.Create("http://localhost:9090/ords/projeto_aebd/io_reads?limit=500");
            WebResponse response = request.GetResponse();
            string json;
            using (var sr = new StreamReader(response.GetResponseStream()))
            {
                json = sr.ReadToEnd();
            }

            JToken ts = JObject.Parse(json).GetValue("items");
            List<io_reads> io = ts.Select(f => f.ToObject<io_reads>()).ToList();

            var list = io.OrderByDescending(X => X.End_time).ToList().Take(10);
            var list1 = list.OrderBy(X => X.Create_date).ToList();
            Chart1.Series.Clear();
            Chart1.Titles.Add("IO Reads/Writes");
            Chart1.Legends.Add("Memory");


            Series series = Chart1.Series.Add("IO_Reads");
            series.ChartType = SeriesChartType.Spline;
            int i = 1;
            foreach (var m in list1)
            {
                series.Points.AddXY(i.ToString(), m.R_value);
                i++;
            }

            ///WRITES

            WebRequest request1 = WebRequest.Create("http://localhost:9090/ords/projeto_aebd/io_writes?limit=500");
            WebResponse response1 = request1.GetResponse();
            string json1;
            using (var sr1 = new StreamReader(response1.GetResponseStream()))
            {
                json1 = sr1.ReadToEnd();
            }

            JToken ts1 = JObject.Parse(json1).GetValue("items");
            List<io_writes> iw = ts1.Select(f => f.ToObject<io_writes>()).ToList();

            var list4 = iw.OrderByDescending(X => X.End_time).ToList().Take(10);
            var list5 = list4.OrderBy(X => X.Create_date).ToList();

            Series series1 = Chart1.Series.Add("IO_Writes");
            series1.ChartType = SeriesChartType.Spline;
            i = 1;
            foreach (var m in list5)
            {
                series1.Points.AddXY(i.ToString(), m.W_value);
                i++;
            }
        }
    }
}