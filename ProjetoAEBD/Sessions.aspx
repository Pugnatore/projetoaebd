﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Sessions.aspx.cs" Inherits="ProjetoAEBD.Sessions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <link href="Content/Plans_template.css" rel="stylesheet" id="bootstrap-css">
     <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
     <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <br />
    <br />
    <br />
    <div class="jumbotron">
       <div class="container text-center">
             <h1 class="page-header">Sessions</h1>
        <p class="lead">Information about sessions</p>
        </div>
    <div class="container ">
	    <div class="row">
    <asp:Repeater ID="RepterDetails" runat="server">  
         <ItemTemplate>  
              
	        <div class="col-md-4 well pricing-table">
	            <div class="pricing-table-holder">
	                <center>
	                    <h3>Session ID: <asp:Label ID="Label3" runat="server" Text='<%#Eval("session_id") %>'/> </h3>
 	                    <p class="caption">
	                       Schema Name: <asp:Label ID="Label1" runat="server" Text='<%#Eval("schema_name") %>'/>
	                    </p>
	                </center>                  
	            </div>
	            
	            <div class="pricing-feature-list">
	                <ul class="list-group">
                      <li class="list-group-item">Username: <asp:Label ID="Label5" runat="server" Text='<%#Eval("username") %>'/></li>
                      <li class="list-group-item">Logon Time: <asp:Label ID="Label2" runat="server" Text='<%#Eval("logon_time") %>'/></li>
                      <li class="list-group-item">Creation Date: <asp:Label ID="Label4" runat="server" Text='<%#Eval("create_date") %>'/></li>
                    </ul>
	            </div>
	        </div>
    </ItemTemplate>  
    </asp:Repeater>
     </div>
	</div>
    </div>
</asp:Content>
