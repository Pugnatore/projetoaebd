﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Tablespaces.aspx.cs" Inherits="ProjetoAEBD.Tablespaces" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
        <link href="Content/Table_template.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <br />
    <br />
    <br />
    
    <div class="jumbotron">
           <%if (His == "0") { %>
       <div class="container text-center">
            <h1 class="page-header">Tablespaces</h1>
            <p class="lead">Information about tablespaces</p>
                        
        </div>
           <div class="wrapper">
        <table id="acrylic">
            <thead>
                <tr>
                    <th>Tablespace Name</th>
                    <th>Size</th>
                    <th>Free Space</th>
                    <th>Used Space</th>
                    <th>Used (%)</th>
                    <th>Creation Date</th>
                </tr>
            </thead>
    <asp:Repeater ID="RepterDetails" runat="server">  
        <ItemTemplate>
            <tbody>
                <tr>
                    <td><%#Eval("tablespace_name") %></td>
                    <td><%#Eval("mem_total") %> MB</td>
                    <td><%#Eval("mem_free") %> MB</td>
                    <td><%#Eval("mem_used") %> MB</td>
                    <td><div class="sml">
                            <div class="pie pie--value pie--disc" style=<%#  "\"--percent:" + Math.Round(Convert.ToDouble(DataBinder.Eval(Container.DataItem, "percentage_used").ToString()))+";\""%>></div> 
                        </div>
                        <%--<div class="progress">
                            <div id="p" class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style=<%#  "\"width:" + DataBinder.Eval(Container.DataItem, "percentage_used").ToString().Replace(",", ".")+"%\""%>></div>
                                <span class="progress-completed"><%#Eval("percentage_used") %>%</span>
                          </div>--%>
                    </td>
                    <td><%#Eval("create_date") %></td>
                </tr>
            </tbody>
         </ItemTemplate> 
    </asp:Repeater>
      </table>
    </div> 
            <%} %>


            <%else { %>
       <div class="container text-center">
            <h1 class="page-header">Tablespaces History</h1>
            <p class="lead">Information about tablespaces history</p>
                        
        </div>
           <div class="wrapper">
            <table id="acrylic">
            <thead>
                <tr>
                    <th>Tablespace Name</th>
                    <th>Size</th>
                    <th>Free Space</th>
                    <th>Used Space</th>
                    <th>Used (%)</th>
                    <th>Alteration Date</th>
                </tr>
            </thead>
        <asp:Repeater ID="Repeater1" runat="server">  
        <ItemTemplate>
            <tbody>
                <tr>
                    <td><%#Eval("tablespace_name") %></td>
                    <td><%#Eval("mem_total") %> MB</td>
                    <td><%#Eval("mem_free") %> MB</td>
                    <td><%#Eval("mem_used") %> MB</td>
                    <td><div class="sml">
                            <div class="pie pie--value pie--disc" style=<%#  "\"--percent:" + Math.Round(Convert.ToDouble(DataBinder.Eval(Container.DataItem, "percentage_used").ToString()))+";\""%>></div> 
                        </div>
                        <%--<div class="progress">
                            <div id="p" class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style=<%#  "\"width:" + DataBinder.Eval(Container.DataItem, "percentage_used").ToString().Replace(",", ".")+"%\""%>></div>
                                <span class="progress-completed"><%#Eval("percentage_used") %>%</span>
                          </div>--%>
                    </td>
                    <td><%#Eval("alter_date") %></td>
                </tr>
            </tbody>
         </ItemTemplate> 
    </asp:Repeater>
      </table>
               </div>
            <%} %>
        </div>

    </asp:Content>
