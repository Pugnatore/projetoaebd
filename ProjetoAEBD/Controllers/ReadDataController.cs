﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace ProjetoAEBD.Controllers
{
    public class ReadDataController : Controller
    {
        public static string connectionstring = "Data Source=oracledb;User Id=SYS;Password=oracle;DBA Privilege=SYSDBA;";
        public static string connectionstring1 = "DATA SOURCE=OracleDB;PERSIST SECURITY INFO=True;USER ID=PROJETO_AEBD;Password=1234;";
        public static string connectionstring_Projeto_Root = "DATA SOURCE=localhost:1521/orcl12c;USER ID=projeto_root;Password=1234;DBA Privilege=SYSDBA;";

        
        static List<datafiles> DataFilesList = new List<datafiles>();
        static List<io_reads> io_ReadsList = new List<io_reads>();
        static List<io_writes> io_WritesList = new List<io_writes>();
        static memory mem = new memory();
        static List<sessions> sessionsList = new List<sessions>();
        static List<tablespaces> tablespacesList = new List<tablespaces>();
        static List<users> usersList = new List<users>();



        // GET: ReadData
        public ActionResult Index()
        {
            return View();
        }

        public static void ReadDataFromSys()
        {
          
            ReadTablespaces();//done
            ReadDataFiles();//done
            ReadUsers();//done
            ReadIOReads();//ver porque se colocar a mesma query no oracle nao retorna nada mas se a executar atraves daqui retorna dois resultados
            ReadIOWrites();//done
            ReadMemory();//done, memory returned
            ReadSessions();//done
            WriteTablespaces();//done
            WriteDataFiles();//done
            WriteUsers();//done
            WriteIOReads();//done
            WriteIOWrites();//done
            WriteMemory();//done
            WriteSessions();//done

        }

        public static void ReadTablespaces()
        {
            
            string queryString = "select\n"+ 
                                  "    df.tablespace_name, round(df.totalspace) as TOTALSPACE, round(NVL((df.totalspace - fs.freespace),df.totalspace - df.freespace)) as ospace, round(NVL(fs.freespace,df.freespace)) as freespace, round(NVL((100 * ((df.totalspace - fs.freespace) / df.totalspace)),(100 * ((df.totalspace - df.freespace) / df.totalspace))),1) as p_usage from\n"+
                                  "     (select tablespace_name, (sum(bytes) / 1048576) as freespace from dba_free_space group by tablespace_name) fs,\n"+
                                  "     (select tablespace_name, (sum(bytes) / 1048576) as totalspace, (sum(bytes) - sum(bytes)) as o_space, (sum(bytes) - sum(bytes)) as freespace, (sum(bytes) - sum(bytes)) as p_usage from dba_data_files group by tablespace_name\n"+
                                  "     UNION\n"+
                                  "     select tablespace_name, sum(TABLESPACE_SIZE) / 1024 / 1024 as totalspace, (((sum(TABLESPACE_SIZE)) - sum(free_space)) / 1024 / 1024) as o_space, (sum(free_space) / 1024 / 1024) as freespace, (100 * (((sum(TABLESPACE_SIZE) / 1024 / 1024) - (sum(free_space) / 1024 / 1024)) / (sum(TABLESPACE_SIZE) / 1024 / 1024))) as p_usage\n"+
                                  "     from dba_temp_free_space\n"+
                                  "     group by tablespace_name ) df\n"+
                                  "     where fs.tablespace_name(+) = df.tablespace_name";
                
            using (OracleConnection connection = new OracleConnection(connectionstring))
            {
                OracleCommand cmd = new OracleCommand(queryString);
                cmd.Connection = connection;
                try
                {
                    connection.Open();

                    OracleDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        tablespaces ts = new tablespaces();
                        ts.Tablespace_name = (string)dr["tablespace_name"];
                        ts.Mem_total = (decimal)dr["totalspace"];
                        ts.Mem_used = (decimal)dr["ospace"];
                        ts.Mem_free = (decimal)dr["freespace"];
                        ts.Percentage_used = (decimal)dr["p_usage"];
                        
                        tablespacesList.Add(ts);
                    }

                    connection.Close();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

        }

        public static void ReadDataFiles()
        {
            
            string queryString =    "SELECT df.file_id \"File ID\",\n" +
                                    "       Substr(df.tablespace_name,1,25) \"Tablespace Name\",\n" +
                                    "        Substr(df.file_name,1,80) \"File Name\",\n" +
                                    "        Round(df.bytes/1024/1024,0) \"Size (M)\",\n" +
                                    "        decode(e.used_bytes,NULL,0,Round(e.used_bytes/1024/1024,0)) \"Used (M)\",\n" +
                                    "        decode(f.free_bytes,NULL,0,Round(f.free_bytes/1024/1024,0)) \"Free (M)\",\n" +
                                    "        decode(e.used_bytes,NULL,0,Round((e.used_bytes/df.bytes)*100,0)) \"% Used\"\n" +
                                    "FROM    DBA_DATA_FILES DF,\n" +
                                    "       (SELECT file_id,\n" +
                                    "               sum(bytes) used_bytes\n" +
                                    "        FROM dba_extents\n" +
                                    "        GROUP by file_id) E,\n" +
                                    "       (SELECT Max(bytes) free_bytes,\n" +
                                    "               file_id\n" +
                                    "        FROM dba_free_space\n" +
                                    "        GROUP BY file_id) f\n" +
                                    "WHERE    e.file_id (+) = df.file_id\n" +
                                    "AND      df.file_id  = f.file_id (+)\n" +
                                    "ORDER BY df.tablespace_name,\n" +
                                    "         df.file_name";
            using (OracleConnection connection = new OracleConnection(connectionstring))
            {
                OracleCommand cmd = new OracleCommand(queryString);
                cmd.Connection = connection;
                try
                {
                    connection.Open();

                    OracleDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        datafiles dt = new datafiles();
                        dt.File_id = Convert.ToInt32(dr["File ID"]);
                        dt.Datafile_name=(string)dr["File name"];
                        dt.Tablespace_name = (string)dr["Tablespace Name"];
                        dt.Mem_total = (decimal)dr["Size (M)"];
                        dt.Mem_used = (decimal)dr["Used (M)"];
                        dt.Mem_free = (decimal)dr["Free (M)"];
                        dt.Percentage_used = (decimal)dr["% Used"];
                        DataFilesList.Add(dt);



                    }

                    connection.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

        }

        public static void ReadUsers()
        {
            
            string queryStringAllUsers = "select user_id, username, default_tablespace,"
                + "temporary_tablespace, account_status from dba_users";
            string querystringuser_quota = "select username, tablespace_name, bytes from dba_ts_quotas";
            //string querystringuser_priv = "select grantee, privilege from dba_sys_privs";
            string querystringactive_user = "SELECT\n" +
                                            "   s.username,\n" +
                                            "   t.sid,\n" +
                                            "   s.serial#,\n" +
                                            "   SUM(VALUE/100) as \"cpu usage (seconds)\"\n" +
                                            "FROM\n" +
                                            "   v$session s,\n" +
                                            "   v$sesstat t,\n" +
                                            "   v$statname n\n" +
                                            "WHERE\n" +
                                            "   t.STATISTIC# = n.STATISTIC#\n" +
                                            "AND\n" +
                                            "   NAME like '%CPU used by this session%'\n" +
                                            "AND\n" +
                                            "   t.SID = s.SID\n" +
                                            "AND\n" +
                                            "   s.username is not null\n" +
                                            "GROUP BY username,t.sid,s.serial#";

            using (OracleConnection connection = new OracleConnection(connectionstring))
            {
                OracleCommand cmd = new OracleCommand(queryStringAllUsers);
                OracleCommand comand = new OracleCommand(querystringuser_quota);
                //OracleCommand comando = new OracleCommand(querystringuser_priv);
                OracleCommand cm = new OracleCommand(querystringactive_user);

                cmd.Connection = connection;
                comand.Connection = connection;
                //comando.Connection = connection;
                cm.Connection = connection;
                try
                {
                    connection.Open();

                    OracleDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        users us = new users();
                        us.User_id = Convert.ToInt32(dr["user_id"]);
                        us.Name = (string)dr["username"];
                        us.Default_tablespace = (string)dr["default_tablespace"];
                        us.Temporary_tablespace= (string)dr["temporary_tablespace"];
                        us.Account_status= (string)dr["account_status"];
                        usersList.Add(us);


                    }

                    OracleDataReader red = comand.ExecuteReader();
                    while (red.Read())
                    {
                        foreach (var item in usersList)
                        {
                            if (item.Name == (string)red["Username"])
                            {
                                item.Quota = Convert.ToInt32(red["bytes"]);
                            }
                        }

                    }

                    /*OracleDataReader read = comando.ExecuteReader();
                    while (read.Read())
                    {
                        foreach (var item in usersList)
                        {
                            if (item.Name == (string)read["grantee"])
                            {
                                string value = (string)read["privilege"];
                                item.Privilege.Add(value);
                            }
                        }

                    }*/

                    OracleDataReader ler = cm.ExecuteReader();
                    while (ler.Read())
                    {
                        foreach (var item in usersList)
                        {
                            if (item.Name == (string)ler["username"])
                            {

                                item.Cpu_usage += Convert.ToDecimal(ler["cpu usage (seconds)"]); //para cada utilizador adicona-lhe a quantidade de cpu usada para obtermos uma quantidade de cpu total
                                /*item.Sid = Convert.ToInt32(ler["sid"]);
                                item.Serial = Convert.ToInt32(ler["serial#"]);*/
                            }
                        }

                    }

                    connection.Close();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }




            }

        }

        public static void ReadIOWrites()
        {
           
            string queryString = "select metric_name, begin_time, end_time, value from v$sysmetric_history "
                    + "where metric_name = 'Physical Writes Per Sec' order by begin_time";
            using (OracleConnection connectio = new OracleConnection(connectionstring_Projeto_Root))
            {
                OracleCommand cmd = new OracleCommand(queryString);
                cmd.Connection = connectio;
                try
                {
                    connectio.Open();

                    OracleDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        io_writes iw = new io_writes();
                        iw.Metric_name = Convert.ToString(dr["metric_name"]);
                        iw.Begin_time = Convert.ToDateTime(dr["begin_time"]);
                        iw.End_time = Convert.ToDateTime(dr["end_time"]);
                        iw.W_value = (decimal)dr["value"];
                        io_WritesList.Add(iw);


                    }


                    connectio.Close();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

        }

        public static void ReadIOReads()
        {
            
            string queryString = "select metric_name,begin_time,end_time,value from v$sysmetric_history "
                  + "where metric_name = 'Physical Reads Per Sec' order by begin_time";
            using (OracleConnection connection = new OracleConnection(connectionstring_Projeto_Root))
            {
                OracleCommand cmd = new OracleCommand(queryString);
                cmd.Connection = connection;
                try
                {
                    connection.Open();

                    OracleDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        io_reads ir = new io_reads();
                        ir.Metric_name = Convert.ToString(dr["metric_name"]);
                        ir.Begin_time = Convert.ToDateTime(dr["begin_time"]);
                        ir.End_time = Convert.ToDateTime(dr["end_time"]);
                        ir.R_value = (decimal)dr["value"];
                        io_ReadsList.Add(ir);


                    }
                    connection.Close();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

        }

        public static memory ReadMemory()
        {
            double total = 0;
            double bytes = 0;
            double percentage = 0;
            string queryString = "select (sga+pga)/1024/1024 as sga_pga from (select sum(value) sga from v$sga), (select sum(pga_used_mem) pga from v$process)";
            using (OracleConnection connection = new OracleConnection(connectionstring))
            {
                OracleCommand cmd = new OracleCommand(queryString);
                cmd.Connection = connection;
                try
                {
                    connection.Open();

                    OracleDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {

                        total = Convert.ToDouble(dr["sga_pga"]);
                    }

                    mem.Total_bytes = total;

                    connection.Clone();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

            }

            string sga2 = "select sum(bytes)/1024/1024 as free_mem from v$sgastat where name = 'free memory'";
            using (OracleConnection connection = new OracleConnection(connectionstring))
            {
                
                OracleCommand cmd = new OracleCommand(sga2);
                cmd.Connection = connection;
                try
                {
                    connection.Open();

                    OracleDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {

                        bytes = Convert.ToDouble(dr["free_mem"]);
                    }


                    connection.Close();


                    mem.Free_bytes = bytes;
                    percentage = (bytes / total)*100;

                    mem.Percentage_free = Math.Round(percentage,2);


                   


                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

            }


            return mem;

        }

        public static void ReadSessions()
        {
            
            string queryString = "SELECT sid, UserName, SchemaName, Logon_Time\n" +
                                 "FROM V$Session\n" +
                                 "WHERE\n" +
                                 "Status='ACTIVE' AND\n" +
                                 "UserName IS NOT NULL";
            using (OracleConnection connection = new OracleConnection(connectionstring))
            {
                OracleCommand cmd = new OracleCommand(queryString);
                cmd.Connection = connection;
                try
                {
                    connection.Open();

                    OracleDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        sessions ss = new sessions();
                        ss.Session_id =Convert.ToInt32(dr["sid"]);
                        ss.Username = Convert.ToString(dr["Username"]);
                        ss.Logon_time = Convert.ToString(dr["Logon_Time"]);
                        ss.Schema_name = Convert.ToString(dr["SchemaName"]);
                        sessionsList.Add(ss);

                    }

                    connection.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

        }

        public static void WriteTablespaces()
        {
            using (OracleConnection connection = new OracleConnection(connectionstring1))
            {
                OracleCommand cmd = new OracleCommand();
                cmd.Connection = connection;
                try
                {
                    connection.Open();
                    
                    foreach (tablespaces t in tablespacesList)
                    {
                        
                        cmd.CommandText = "UPDATE tablespaces "
                                    + "SET mem_used = " + t.Mem_used + ","
                                    + " mem_free = " + t.Mem_free + ","
                                    + " mem_total = " + t.Mem_total + ","
                                    + " percentage_used = " + t.Percentage_used.ToString().Replace(",", ".") + ","
                                    + " create_date = CURRENT_TIMESTAMP"
                                    + " WHERE tablespace_name = " + "'" + t.Tablespace_name + "'";

                        int rowsUpdated = cmd.ExecuteNonQuery();

                        if (rowsUpdated == 0)
                        {
                            cmd.CommandText = "INSERT INTO tablespaces(tablespace_name, mem_used, mem_free, mem_total, percentage_used, create_date)" +
                                " VALUES("
                                + "'" + t.Tablespace_name + "'" + ","
                                + t.Mem_used + ","
                                + t.Mem_free + ","
                                + t.Mem_total + ","
                                + t.Percentage_used.ToString().Replace(",", ".") + ","
                                + "CURRENT_TIMESTAMP)";

                            cmd.ExecuteNonQuery();
                        }

                    }
                    connection.Close();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
        
        public static void WriteDataFiles()
        {
            using (OracleConnection connection = new OracleConnection(connectionstring1))
            {
                OracleCommand cmd = new OracleCommand();
                cmd.Connection = connection;
                try
                {
                    connection.Open();

                    foreach (datafiles d in DataFilesList)
                    {

                        cmd.CommandText = "UPDATE datafiles SET "
                                        + " mem_total = " + d.Mem_total + ","
                                        + " mem_used = " + d.Mem_used + ","
                                        + " mem_free = " + d.Mem_free + ","
                                        + " percentage_used = " + d.Percentage_used + ","
                                        + " create_date = CURRENT_TIMESTAMP"
                                        + " WHERE file_id = " + d.File_id;

                        int rowsUpdated = cmd.ExecuteNonQuery();
                        if (rowsUpdated == 0)
                        {
                            cmd.CommandText = "INSERT INTO datafiles(file_id, datafile_name, tablespace_name, mem_total, mem_used, mem_free, percentage_used, create_date)"
                                    + "VALUES("
                                    + d.File_id + ","
                                    + "'" + d.Datafile_name + "'" + ","
                                    + "'" + d.Tablespace_name + "'" + ","
                                    + d.Mem_total + ","
                                    + d.Mem_used + ","
                                    + d.Mem_free + ","
                                    + d.Percentage_used + ","
                                    + "CURRENT_TIMESTAMP)";
                            cmd.ExecuteNonQuery();
                        }
                    }
                    connection.Close();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        public static void WriteUsers()
        {
            using (OracleConnection connection = new OracleConnection(connectionstring1))
            {
                OracleCommand cmd = new OracleCommand();
                cmd.Connection = connection;
                try
                {
                    connection.Open();

                    foreach(users u in usersList)
                    {

                        cmd.CommandText = "UPDATE users " +
                                          "SET ACCOUNT_STATUS= " + "'" + u.Account_status + "'" + ","
                                        + "CPU_USAGE= " + u.Cpu_usage.ToString().Replace(",", ".") + ","
                                        + "CREATE_DATE = CURRENT_TIMESTAMP "
                                        + " WHERE USER_ID= " + u.User_id;

                        int rowsUpdated = cmd.ExecuteNonQuery();
                        if (rowsUpdated == 0)
                        {
                            cmd.CommandText = "INSERT INTO users(user_id, name, default_tablespace, temporary_tablespace,"
                                + "account_status, quota, cpu_usage, create_date)"
                                + "VALUES("
                                + u.User_id + ","
                                + "'" + u.Name + "'" + ","
                                + "'" + u.Default_tablespace + "'" + ","
                                + "'" + u.Temporary_tablespace + "'" + ","
                                + "'" + u.Account_status + "'" + ","
                                + u.Quota + ","
                                //+ "'" + u.Privilege + "'" + ","
                                + u.Cpu_usage.ToString().Replace(",", ".") + ","
                                + "CURRENT_TIMESTAMP)";
                            cmd.ExecuteNonQuery();
                        }
                    }
                    connection.Close();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
        
        public static void WriteIOReads()
       {
        using (OracleConnection connection = new OracleConnection(connectionstring1))
        {
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = connection;
            try
            {
                connection.Open();

                    foreach(io_reads ir in io_ReadsList)
                    {
                        cmd.CommandText = "INSERT INTO io_reads(metric_name,begin_time,end_time,r_value,create_date)"
                                   + " VALUES("
                                   + "'" + ir.Metric_name + "'" + ","
                                   + "to_timestamp('" + ir.Begin_time + "', 'dd/mm/yyyy hh24:mi:ss')" + ","
                                   + "to_timestamp('" + ir.End_time + "', 'dd/mm/yyyy hh24:mi:ss')" + ","
                                   + ir.R_value.ToString().Replace(",", ".") + ","
                                   + "CURRENT_TIMESTAMP)";
                        cmd.ExecuteNonQuery();
                    }
                    connection.Close();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }

        public static void WriteIOWrites()
        {
            using (OracleConnection connection = new OracleConnection(connectionstring1))
            {
                OracleCommand cmd = new OracleCommand();
                cmd.Connection = connection;
                try
                {
                    connection.Open();

                    foreach(io_writes iw in io_WritesList)
                    {
                        cmd.CommandText = "INSERT INTO io_writes(metric_name,begin_time,end_time,w_value,create_date)"
                                   + "VALUES("
                                   + "'" + iw.Metric_name + "'" + ","
                                   + "to_timestamp('" + iw.Begin_time + "', 'dd/mm/yyyy hh24:mi:ss')" + ","
                                   + "to_timestamp('" + iw.End_time + "', 'dd/mm/yyyy hh24:mi:ss')" + ","
                                   + iw.W_value.ToString().Replace(",", ".") + ","
                                   + "CURRENT_TIMESTAMP) ";
                        cmd.ExecuteNonQuery();
                    }
                    connection.Close();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        public static void WriteMemory()
        {
            using (OracleConnection connection = new OracleConnection(connectionstring1))
            {
                OracleCommand cmd = new OracleCommand();
                cmd.Connection = connection;
                try
                {
                    connection.Open();

                    cmd.CommandText = "INSERT INTO memory(total_bytes,free_bytes, percentage_free, create_date)"
                     + " VALUES("
                     + Math.Round(mem.Total_bytes).ToString().Replace(",", ".") + ","
                     + Math.Round(mem.Free_bytes).ToString().Replace(",", ".") + ","
                     + mem.Percentage_free.ToString().Replace(",", ".") + ","
                     + "CURRENT_TIMESTAMP)";

                    cmd.ExecuteNonQuery();

                    connection.Close();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        public static void WriteSessions()
        {
            using (OracleConnection connection = new OracleConnection(connectionstring1))
            {
                OracleCommand cmd = new OracleCommand();
                cmd.Connection = connection;
                try
                {
                    connection.Open();

                    foreach (sessions s in sessionsList)
                    {

                        cmd.CommandText = "UPDATE sessions SET"
                                        + " logon_time = " + "'" + s.Logon_time + "'" + ","
                                        + " create_date = CURRENT_TIMESTAMP"
                                        + " WHERE session_id = " + s.Session_id;


                        int rowsUpdated = cmd.ExecuteNonQuery();

                        if (rowsUpdated == 0)
                        {
                            cmd.CommandText = "INSERT INTO sessions(session_id, username, user_id, schema_name, logon_time, create_date)"
                                    + " VALUES("
                                    + s.Session_id + ","
                                    + "'" + s.Username + "'" + ","
                                    + s.User_id + ","
                                    + "'" + s.Schema_name + "'" + ","
                                    + "'" + s.Logon_time + "'" + ","
                                    + "CURRENT_TIMESTAMP)";
                            cmd.ExecuteNonQuery();
                        }
                    }
                    connection.Close();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
}