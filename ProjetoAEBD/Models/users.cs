﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjetoAEBD
{
    public class users
    {
        int user_id;
        string name;
        string default_tablespace;
        string temporary_tablespace;
        string account_status;
        int quota;
        decimal cpu_usage;
        DateTime create_date;

        public int User_id { get => user_id; set => user_id = value; }
        public string Name { get => name; set => name = value; }
        public string Default_tablespace { get => default_tablespace; set => default_tablespace = value; }
        public string Temporary_tablespace { get => temporary_tablespace; set => temporary_tablespace = value; }
        public string Account_status { get => account_status; set => account_status = value; }
        public int Quota { get => quota; set => quota = value; }
        public decimal Cpu_usage { get => cpu_usage; set => cpu_usage = value; }
        public DateTime Create_date { get => create_date; set => create_date = value; }
    }
}