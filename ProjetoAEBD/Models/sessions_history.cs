﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjetoAEBD.Models
{
    public class sessions_history
    {
        int session_id;
        string username;
        int user_id;
        string schema_name;
        string logon_time;
        DateTime alter_date;

        public int Session_id { get => session_id; set => session_id = value; }
        public string Username { get => username; set => username = value; }
        public int User_id { get => user_id; set => user_id = value; }
        public string Schema_name { get => schema_name; set => schema_name = value; }
        public string Logon_time { get => logon_time; set => logon_time = value; }
        public DateTime Alter_date { get => alter_date; set => alter_date = value; }
    }
}