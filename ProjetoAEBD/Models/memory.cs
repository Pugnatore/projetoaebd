﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjetoAEBD
{
    public class memory
    {
        double total_bytes;
        double free_bytes;
        double percentage_free;
        DateTime create_date;

        public double Total_bytes { get => total_bytes; set => total_bytes = value; }
        public double Free_bytes { get => free_bytes; set => free_bytes = value; }
        public double Percentage_free { get => percentage_free; set => percentage_free = value; }
        public DateTime Create_date { get => create_date; set => create_date = value; }
    }
}