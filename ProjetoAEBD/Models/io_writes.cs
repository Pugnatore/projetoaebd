﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjetoAEBD
{
    public class io_writes
    {
        string metric_name;
        DateTime begin_time;
        DateTime end_time;
        decimal w_value;
        DateTime create_date;

        public string Metric_name { get => metric_name; set => metric_name = value; }
        public DateTime Begin_time { get => begin_time; set => begin_time = value; }
        public DateTime End_time { get => end_time; set => end_time = value; }
        public decimal W_value { get => w_value; set => w_value = value; }
        public DateTime Create_date { get => create_date; set => create_date = value; }
    }
}