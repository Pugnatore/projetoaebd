﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjetoAEBD.Models
{
    public class tablespaces_history
    {
        string tablespace_name;
        decimal mem_total;
        decimal mem_free;
        decimal mem_used;
        decimal percentage_used;
        DateTime alter_date;

        public string Tablespace_name { get => tablespace_name; set => tablespace_name = value; }
        public decimal Mem_used { get => mem_used; set => mem_used = value; }
        public decimal Mem_free { get => mem_free; set => mem_free = value; }
        public decimal Mem_total { get => mem_total; set => mem_total = value; }
        public decimal Percentage_used { get => percentage_used; set => percentage_used = value; }
        public DateTime Alter_date { get => alter_date; set => alter_date = value; }
    }
}