﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="IO_RW.aspx.cs" Inherits="ProjetoAEBD.IO_RW" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
               <link href="Content/Table_template.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <br />
    <br />
    <br />
    <div class="jumbotron">
       <div class="container text-center">
             <h1 class="page-header">IO Reads/Writes</h1>
        <p class="lead">Information about IO Reads/Writes</p>
        </div>

               <asp:Chart ID="Chart1" runat="server" OnLoad="Chart1_Load" height= "450px" width="550px">
                   <series>
                       <asp:Series ChartType="Spline" Name="Series1">
                       </asp:Series>
                   </series>
                   <chartareas>
                       <asp:ChartArea Name="ChartArea1">
                           <AxisY Title="Physical Writes/Reads Per Sec"></AxisY>
                           <AxisX Title="Last 10 Records" IsLabelAutoFit="True"><LabelStyle Angle="-90" Interval="1" /></AxisX>
                       </asp:ChartArea>
                   </chartareas>
               </asp:Chart>
        	</div>
</asp:Content>
