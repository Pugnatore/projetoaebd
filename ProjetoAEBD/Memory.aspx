﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Memory.aspx.cs" Inherits="ProjetoAEBD.Memory" %>
<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
               <link href="Content/Table_template.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <br />
    <br />
    <br />
    <div class="jumbotron">
       <div class="container text-center">
             <h1 class="page-header">Memory</h1>
        <p class="lead">Information about memory</p>
        <hr />
          <h2>Last read</h2>
              <div class="wrapper">
            <table id="acrylic">
            <thead>
                <tr>
                    <th>Read Date</th>
                    <th>Total Space</th>
                    <th>Free Space</th>
                    <th>Used (%)</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><%= date %></td>
                    <td><%= total%> MB</td>
                    <td><%= free%> MB</td>
                    <td><div class="sml">
                           <div class="pie pie--value pie--disc" style=<%=  "\"--percent:" + Math.Round(used)+";\""%>></div> 
                         </div>
                    </td>
                </tr>
            </tbody>
      </table>
               </div>
           <hr />
        <h2>Variation in the percentage of free memory</h2>
               <asp:Chart ID="Chart1" runat="server" OnLoad="Chart1_Load" height= "450px" width="550px">
                   <series>
                       <asp:Series ChartType="Spline" Name="Series1">
                       </asp:Series>
                   </series>
                   <chartareas>
                       <asp:ChartArea Name="ChartArea1">
                           <AxisY Title="Percentage Free (%)"></AxisY>
                           <AxisX Title="Last Records" IsLabelAutoFit="True"><LabelStyle Angle="-90" Interval="1" /></AxisX>
                       </asp:ChartArea>
                   </chartareas>
               </asp:Chart>
        	</div>
</asp:Content>
