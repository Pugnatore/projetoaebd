﻿using Newtonsoft.Json.Linq;
using ProjetoAEBD.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProjetoAEBD
{
    public partial class SessionsHistory : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            WebRequest request = WebRequest.Create("http://localhost:9090/ords/projeto_aebd/sessionshistory?limit=500");
            WebResponse response = request.GetResponse();
            string json;
            using (var sr = new StreamReader(response.GetResponseStream()))
            {
                json = sr.ReadToEnd();
            }

            JToken ts = JObject.Parse(json).GetValue("items");
            List<sessions_history> tsp = ts.Select(f => f.ToObject<sessions_history>()).ToList();
            var list = tsp.OrderBy(x => x.Session_id).ToList();
            RepterDetails.DataSource = list;
            RepterDetails.DataBind();
        }
    }
}