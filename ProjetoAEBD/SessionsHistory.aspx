﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SessionsHistory.aspx.cs" Inherits="ProjetoAEBD.SessionsHistory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
        <link href="Content/Table_template.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <br />
    <br />
    <br />
    <div class="jumbotron">
       <div class="container text-center">
             <h1 class="page-header">Sessions History</h1>
        <p class="lead">Information about sessions history</p>
        </div>
<div class="wrapper">
        <table id="acrylic">
            <thead>
                <tr>
                    <th>Session ID</th>
                    <th>Username</th>
                    <th>Schema Name</th>
                    <th>Logon Time</th>
                    <th>Alter Date</th>
                </tr>
            </thead>
    <asp:Repeater ID="RepterDetails" runat="server">  
        <ItemTemplate>
            <tbody>
                <tr>
                    <td><%#Eval("session_id") %></td>
                    <td><%#Eval("username") %></td>
                    <td><%#Eval("schema_name") %></td>
                    <td><%#Eval("logon_time") %></td>
                    <td><%#Eval("alter_date") %></td>
                </tr>
            </tbody>
         </ItemTemplate> 
    </asp:Repeater>
      </table>
    </div> 
    </div> 
</asp:Content>
