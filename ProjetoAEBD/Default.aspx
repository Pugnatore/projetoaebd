﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ProjetoAEBD._Default" %>

<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <link href="Content/Site.css" rel="stylesheet" />
    <img src="Content/Index.png"  style="width: 1350px;margin-left: -90px;margin-top:  -20px;height: 500px;" />
    <div class="jumbotron" style="margin-left: -100px; width: 1350px;margin-top:  -50px;">
    </div>
    <div>
        <br />
    <div class="container">
        <div class="row">
             <div class="container">
        <div class="row">
                   <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="thumbnail img-thumb-bg" style="background-image: url(Content/Tablespaces.png); background-size: 400px; border-radius: 25px ">
                        <div class="overlay"></div>
                        <div class="caption">
                            <div class="tag"><a href="Tablespaces.aspx?historico=0">AEBD</a></div>
                            <div class="title"><a href="Tablespaces.aspx?historico=0">Tablespaces</a></div>
                            <div class="content">
                                <p>Information about tablespaces</p>
                            </div>
                        </div>
                    </div>
                </div>
             <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="thumbnail img-thumb-bg" style="background-image: url(Content/Datafiles.png); background-size: 400px; border-radius: 25px ">
                        <div class="overlay"></div>
                        <div class="caption">
                            <div class="tag"><a href="Datafiles.aspx?historico=0">AEBD</a></div>
                            <div class="title"><a href="Datafiles.aspx?historico=0">Datafiles</a></div>
                            <div class="content">
                                <p>Information about datafiles</p>
                            </div>
                        </div>
                    </div>
                </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="thumbnail img-thumb-bg" style="background-image: url(Content/Users.png); background-size: 400px; border-radius: 25px ">
                        <div class="overlay"></div>
                        <div class="caption">
                            <div class="tag"><a href="Users.aspx?historico=0">AEBD</a></div>
                            <div class="title"><a href="Users.aspx?historico=0">Users</a></div>
                            <div class="content">
                                <p>Information about users</p>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
     </div>
    </div>
    <div class="container">
        <div class="row">
             <div class="container">
        <div class="row">
                   <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="thumbnail img-thumb-bg" style="background-image:url(Content/IO_ReadsWrites.png); background-size: 400px; border-radius: 25px ">
                        <div class="overlay"></div>
                        <div class="caption">
                            <div class="tag"><a href="IO_RW.aspx">AEBD</a></div>
                            <div class="title"><a href="IO_RW.aspx">IO Reads and IO Writes</a></div>
                            <div class="content">
                                <p>Information about IO Reads and IO Writes</p>
                            </div>
                        </div>
                    </div>
                </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="thumbnail img-thumb-bg" style="background-image:url(Content/Memory.png); background-size: 400px; border-radius: 25px ">
                        <div class="overlay"></div>
                        <div class="caption">
                            <div class="tag"><a href="Memory.aspx">AEBD</a></div>
                            <div class="title"><a href="Memory.aspx">Memory</a></div>
                            <div class="content">
                                <p>Information about memory</p>
                            </div>
                        </div>
                    </div>
                </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="thumbnail img-thumb-bg" style="background-image:url(Content/Sessions.png); background-size: 400px; border-radius: 25px ">
                        <div class="overlay"></div>
                        <div class="caption">
                            <div class="tag"><a href="Sessions.aspx">AEBD</a></div>
                            <div class="title"><a href="Sessions.aspx">Sessions</a></div>
                            <div class="content">
                                <p>Information about sessions</p>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
     </div>
    </div>
        <br />
        <div align="center"><h1>History</h1></div>
        <br />
          <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="thumbnail img-thumb-bg" style="background-image: url(Content/Tablespaces_history.png); background-size: 400px; border-radius: 25px ">
                        <div class="overlay"></div>
                        <div class="caption">
                            <div class="tag"><a href="Tablespaces.aspx?historico=1">AEBD</a></div>
                            <div class="title"><a href="Tablespaces.aspx?historico=1">Tablespaces History</a></div>
                            <div class="content">
                                <p>Information about tablespaces history</p>
                            </div>
                        </div>
                    </div>
                </div>
         <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="thumbnail img-thumb-bg" style="background-image: url(Content/Datafiles_history.png); background-size: 400px; border-radius: 25px ">
                        <div class="overlay"></div>
                        <div class="caption">
                            <div class="tag"><a href="Datafiles.aspx?historico=1">AEBD</a></div>
                            <div class="title"><a href="Datafiles.aspx?historico=1">Datafiles History</a></div>
                            <div class="content">
                                <p>Information about datafiles history</p>
                            </div>
                        </div>
                    </div>
                </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="thumbnail img-thumb-bg" style="background-image: url(Content/Users_history.png); background-size: 400px; border-radius: 25px ">
                        <div class="overlay"></div>
                        <div class="caption">
                            <div class="tag"><a href="Users.aspx?historico=1">AEBD</a></div>
                            <div class="title"><a href="Users.aspx?historico=1">Users History</a></div>
                            <div class="content">
                                <p>Information about users history</p>
                            </div>
                        </div>
                    </div>
                </div>
         <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="thumbnail img-thumb-bg" style="background-image:url(Content/Sessions_history.png); background-size: 400px; border-radius: 25px ">
                        <div class="overlay"></div>
                        <div class="caption">
                            <div class="tag"><a href="SessionsHistory.aspx">AEBD</a></div>
                            <div class="title"><a href="SessionsHistory.aspx">Sessions History</a></div>
                            <div class="content">
                                <p>Information about sessions history</p>
                            </div>
                        </div>
                    </div>
                </div>
    </div>
</asp:Content>
