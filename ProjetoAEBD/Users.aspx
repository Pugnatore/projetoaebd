﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Users.aspx.cs" Inherits="ProjetoAEBD.Users" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
        <link href="Content/Table_template.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <br />
    <br />
    <br />
    <div class="jumbotron">
        <%if (His == "0") { %>
       <div class="container text-center">
             <h1 class="page-header">Users</h1>
        <p class="lead">Information about users</p>
        </div>
<div class="wrapper">
        <table id="acrylic">
            <thead>
                <tr>
                    <th>Username</th>
                    <th>Account Status</th>
                    <th>Default Tablespace</th>
                    <th>Temporary Tablespace</th>
                    <th>Quota</th>
                    <th>CPU Usage</th>
                    <th>Creation Date</th>
                </tr>
            </thead>
    <asp:Repeater ID="RepterDetails" runat="server">  
        <ItemTemplate>
            <tbody>
                <tr>
                    <td><%#Eval("name") %></td>
                    <td><%#Eval("account_status") %></td>
                    <td><%#Eval("default_tablespace") %></td>
                    <td><%#Eval("temporary_tablespace") %></td>
                    <td><%#Eval("quota") %> </td>
                    <td><%#Eval("cpu_usage") %> </td>
                    <td><%#Eval("create_date") %></td>
                </tr>
            </tbody>
         </ItemTemplate> 
    </asp:Repeater>
      </table>
    </div> 
       <%} %>


        <%else { %>
               <div class="container text-center">
             <h1 class="page-header">Users History</h1>
        <p class="lead">Information about users history</p>
        </div>
<div class="wrapper">
        <table id="acrylic">
            <thead>
                <tr>
                    <th>Username</th>
                    <th>Account Status</th>
                    <th>Default Tablespace</th>
                    <th>Temporary Tablespace</th>
                    <th>Quota</th>
                    <th>CPU Usage</th>
                    <th>Alteration Date</th>
                </tr>
            </thead>
    <asp:Repeater ID="Repeater1" runat="server">  
        <ItemTemplate>
            <tbody>
                <tr>
                    <td><%#Eval("name") %></td>
                    <td><%#Eval("account_status") %></td>
                    <td><%#Eval("default_tablespace") %></td>
                    <td><%#Eval("temporary_tablespace") %></td>
                    <td><%#Eval("quota") %> </td>
                    <td><%#Eval("cpu_usage") %> </td>
                    <td><%#Eval("alter_date") %></td>
                </tr>
            </tbody>
         </ItemTemplate> 
    </asp:Repeater>
      </table>
    </div> 
         <%} %>
    </div>
</asp:Content>
