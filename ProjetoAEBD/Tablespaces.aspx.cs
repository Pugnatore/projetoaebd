﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProjetoAEBD.Controllers;
using ProjetoAEBD.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using static System.Net.WebRequestMethods;

namespace ProjetoAEBD
{
    public partial class Tablespaces : System.Web.UI.Page
    {
        public string His { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            His= Request.QueryString["historico"];
            if (His == "0")
            {
                WebRequest request = WebRequest.Create("http://localhost:9090/ords/projeto_aebd/tablespaces?limit=500");
                WebResponse response = request.GetResponse();
                string json;
                using (var sr = new StreamReader(response.GetResponseStream()))
                {
                    json = sr.ReadToEnd();
                }

                JToken ts = JObject.Parse(json).GetValue("items");
                List<tablespaces> tsp = ts.Select(f => f.ToObject<tablespaces>()).ToList();
                var list = tsp.OrderBy(x => x.Tablespace_name).ToList();
                RepterDetails.DataSource = list;
                RepterDetails.DataBind();
            }
            else
            {
                WebRequest request = WebRequest.Create("http://localhost:9090/ords/projeto_aebd/tablespaceshistory?limit=500");
                WebResponse response = request.GetResponse();
                string json;
                using (var sr = new StreamReader(response.GetResponseStream()))
                {
                    json = sr.ReadToEnd();
                }

                JToken ts = JObject.Parse(json).GetValue("items");
                List<tablespaces_history> tsp = ts.Select(f => f.ToObject<tablespaces_history>()).ToList();
                var list = tsp.OrderBy(x => x.Tablespace_name).ToList();
                Repeater1.DataSource = list;
                Repeater1.DataBind();
            }
        }
    }
}