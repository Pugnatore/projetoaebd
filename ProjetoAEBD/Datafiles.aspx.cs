﻿using Newtonsoft.Json.Linq;
using ProjetoAEBD.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProjetoAEBD
{
    public partial class Datafiles : System.Web.UI.Page
    {
        public string His { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            His = Request.QueryString["historico"];
            if (His == "0")
            {
                WebRequest request = WebRequest.Create("http://localhost:9090/ords/projeto_aebd/datafiles?limit=500");
                WebResponse response = request.GetResponse();
                string json;
                using (var sr = new StreamReader(response.GetResponseStream()))
                {
                    json = sr.ReadToEnd();
                }

                JToken ts = JObject.Parse(json).GetValue("items");
                List<datafiles> tsp = ts.Select(f => f.ToObject<datafiles>()).ToList();
                var list = tsp.OrderBy(x => x.Tablespace_name).ToList();
                RepterDetails.DataSource = list;
                RepterDetails.DataBind();
            }
            else
            {
                WebRequest request = WebRequest.Create("http://localhost:9090/ords/projeto_aebd/datafileshistory?limit=500");
                WebResponse response = request.GetResponse();
                string json;
                using (var sr = new StreamReader(response.GetResponseStream()))
                {
                    json = sr.ReadToEnd();
                }

                JToken ts = JObject.Parse(json).GetValue("items");
                List<datafiles_history> tsp = ts.Select(f => f.ToObject<datafiles_history>()).ToList();
                var list = tsp.OrderBy(x => x.Tablespace_name).ToList();
                Repeater1.DataSource = list;
                Repeater1.DataBind();
            }
        }
    }
}