﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;

namespace ProjetoAEBD
{
    public partial class Memory : System.Web.UI.Page
    {
        public double total { get; set; }
        public double used { get; set; }
        public double free { get; set; }
        public DateTime date { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void Chart1_Load(object sender, EventArgs e)
        {
            WebRequest request = WebRequest.Create("http://localhost:9090/ords/projeto_aebd/memory?limit=500");
            WebResponse response = request.GetResponse();
            string json;
            using (var sr = new StreamReader(response.GetResponseStream()))
            {
                json = sr.ReadToEnd();
            }

            JToken ts = JObject.Parse(json).GetValue("items");
            List<memory> tsp = ts.Select(f => f.ToObject<memory>()).ToList();

            var list = tsp.OrderByDescending(X => X.Create_date).ToList().Take(10);
            var list1 = list.OrderBy(X => X.Create_date).ToList();
            Chart1.Series.Clear();
            Chart1.Titles.Add("Memory");
            Chart1.Legends.Add("Memory");


            Series series = Chart1.Series.Add("Free (%)");
            series.ChartType = SeriesChartType.Spline;
            int i = 1;
            foreach(var m in list1)
            {
                series.Points.AddXY(i.ToString(), m.Percentage_free);
                i++;
            }

            total = list1.Last().Total_bytes;
            used = Math.Round(((list1.Last().Total_bytes - list1.Last().Free_bytes)/ list1.Last().Total_bytes)*100);
            free = list1.Last().Free_bytes;
            date = list1.Last().Create_date;
        }
    }
}